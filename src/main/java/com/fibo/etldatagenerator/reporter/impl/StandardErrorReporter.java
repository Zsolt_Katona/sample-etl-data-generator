package com.fibo.etldatagenerator.reporter.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.fibo.etldatagenerator.reporter.Reporter;

public class StandardErrorReporter implements Reporter {

	@Override
	public void debug(String message) {
		printMessage("DEBUG", message);
	}

	@Override
	public void info(String message) {
		printMessage("INFO", message);
	}

	@Override
	public void warning(String message) {
		printMessage("WARN", message);
	}

	@Override
	public void error(String message, Exception e) {
		printMessage("ERROR", message);
		e.printStackTrace(System.err);
	}
	
	public void printMessage(String level, String message) {
		System.err.println(
			String.format("%s %-5s %s",
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()),
				level, 
				message
			)
		);
	}

}

package com.fibo.etldatagenerator.reporter;

import com.fibo.etldatagenerator.reporter.impl.StandardErrorReporter;

public class ReporterFacade {

	public static Reporter reporter = new StandardErrorReporter();
	
	public static void debug(String message) {
		reporter.debug(message);
	}

	public static void info(String message) {
		reporter.info(message);
	}

	public static void warning(String message) {
		reporter.warning(message);
	}

	public static void error(String message, Exception e) {
		reporter.error(message, e);
	}
	
}

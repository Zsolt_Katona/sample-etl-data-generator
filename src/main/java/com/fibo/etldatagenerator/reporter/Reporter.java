package com.fibo.etldatagenerator.reporter;

public interface Reporter {

	public void debug(String message);
	
	public void info(String message);
	
	public void warning(String message);
	
	public void error(String message, Exception e);
	
}

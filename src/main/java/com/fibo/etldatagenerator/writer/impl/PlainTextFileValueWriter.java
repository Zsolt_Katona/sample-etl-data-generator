package com.fibo.etldatagenerator.writer.impl;

import java.io.FileWriter;
import java.io.IOException;

import com.fibo.etldatagenerator.writer.ValueWriter;

public class PlainTextFileValueWriter implements ValueWriter {

	private FileWriter fileWriter;
	
	public PlainTextFileValueWriter(String outputFilePath) {
		try {
			fileWriter = new FileWriter(outputFilePath);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void write(Object record) {
		try {
			fileWriter.write(String.valueOf(record));
			fileWriter.write(System.lineSeparator());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void close() throws IOException {
		fileWriter.close();
	}

}

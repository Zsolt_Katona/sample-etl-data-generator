package com.fibo.etldatagenerator.writer.impl;

import java.io.IOException;

import com.fibo.etldatagenerator.writer.ValueWriter;

public class ConsoleValueWriter implements ValueWriter {

	@Override
	public void write(Object record) {
		System.out.println(record);
	}

	@Override
	public void close() throws IOException {
		
	}

}

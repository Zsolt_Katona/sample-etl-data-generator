package com.fibo.etldatagenerator.writer.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import com.fibo.etldatagenerator.config.format.CSVFormat;
import com.fibo.etldatagenerator.generator.record.RecordGenerator;
import com.fibo.etldatagenerator.generator.record.Row;
import com.fibo.etldatagenerator.generator.record.impl.RowGenerator;
import com.fibo.etldatagenerator.reporter.ReporterFacade;
import com.fibo.etldatagenerator.writer.RowWriter;

import au.com.bytecode.opencsv.CSVWriter;

public class CSVRowWriter implements RowWriter {

	private String outputFilePath;
	private CSVWriter writer;

	private CSVFormat format;
	
	public CSVRowWriter(String outputFilePath, CSVFormat format) {
		try {
			this.outputFilePath = outputFilePath;
			this.format = format;
			this.writer = new CSVWriter(new FileWriter(outputFilePath), toChar(format.getSeparator()),
					toChar(format.getQuoteCharacter()), toChar(format.getEscapeCharacter()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private char toChar(Character value) {
		return value != null ? value.charValue() : '\0';
	}
	
	@Override
	public void initialize(RecordGenerator<Row> recordGenerator) {
		if (format.isUseHeader()) {
			write(((RowGenerator) recordGenerator).headerRow());
		}
	}

	@Override
	public void write(Row record) {
		if (format.getNullRepresentation() != null) {
			writer.writeNext(Arrays.stream(record.getValues()).map(n -> n == null ? format.getNullRepresentation() : n).toArray(size -> new String[size]));
		} else {
			writer.writeNext(record.getValues());
		}
	}

	@Override
	public void close() throws IOException {
		writer.close();
		ReporterFacade.info(String.format("CSV file [%s] successfully created.", outputFilePath));
	}

}

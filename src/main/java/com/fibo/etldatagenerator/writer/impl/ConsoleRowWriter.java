package com.fibo.etldatagenerator.writer.impl;

import java.io.IOException;
import java.util.Arrays;

import com.fibo.etldatagenerator.generator.record.Row;
import com.fibo.etldatagenerator.writer.RowWriter;

public class ConsoleRowWriter implements RowWriter {

	@Override
	public void write(Row row) {
		System.out.println(Arrays.toString(row.getValues()));
	}

	@Override
	public void close() throws IOException {
		// Nothing
	}

}

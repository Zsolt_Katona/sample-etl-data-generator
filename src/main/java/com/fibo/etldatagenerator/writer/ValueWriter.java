package com.fibo.etldatagenerator.writer;

import com.fibo.etldatagenerator.generator.record.RecordGenerator;

public interface ValueWriter extends RecordWriter<Object> {

	@Override
	default void initialize(RecordGenerator<Object> recordGenerator) {
		// Nothing
	}

}

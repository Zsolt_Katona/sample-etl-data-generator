package com.fibo.etldatagenerator.writer;

import com.fibo.etldatagenerator.generator.record.RecordGenerator;
import com.fibo.etldatagenerator.generator.record.Row;
import com.fibo.etldatagenerator.generator.record.impl.RowGenerator;

public interface RowWriter extends RecordWriter<Row> {

	@Override
	default void initialize(RecordGenerator<Row> recordGenerator) {
		write(((RowGenerator) recordGenerator).headerRow());
	}

}

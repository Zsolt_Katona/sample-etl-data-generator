package com.fibo.etldatagenerator.writer;

import java.io.Closeable;

import com.fibo.etldatagenerator.generator.record.RecordGenerator;

public interface RecordWriter<T> extends Closeable {

	void initialize(RecordGenerator<T> recordGenerator);
	
	void write(T record);
	
}

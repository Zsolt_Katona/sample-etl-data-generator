package com.fibo.etldatagenerator.config.job;

import com.fibo.etldatagenerator.generator.record.RecordGenerator;
import com.fibo.etldatagenerator.reporter.ReporterFacade;
import com.fibo.etldatagenerator.writer.RecordWriter;

public class JobEntry<T> {
	
	private String description;
	
	private RecordGenerator<T> recordGenerator;
	
	private RecordWriter<T> recordWriter;

	private int maxRecords = 0;

	public JobEntry() {
	}

	public JobEntry(String description, RecordGenerator<T> recordGenerator, RecordWriter<T> recordWriter,
			int maxRecords) {
		this.description = description;
		this.recordGenerator = recordGenerator;
		this.recordWriter = recordWriter;
		this.maxRecords = maxRecords;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setRecordGenerator(RecordGenerator<T> recordGenerator) {
		this.recordGenerator = recordGenerator;
	}

	public RecordWriter<T> getRecordWriter() {
		return recordWriter;
	}

	public void setRecordWriter(RecordWriter<T> recordWriter) {
		this.recordWriter = recordWriter;
	}

	public void setMaxRecords(int maxRecords) {
		this.maxRecords = maxRecords;
	}

	public RecordGenerator<T> getRecordGenerator() {
		return recordGenerator;
	}

	public void execute() {
		try {
			recordGenerator.initialize();
			recordWriter.initialize(recordGenerator);
			boolean maxRecordsReached = false;
			for (int i = 0; i < maxRecords; ++i) {
				if (recordGenerator.hasNext()) {
					recordWriter.write(recordGenerator.next());
				} else {
					maxRecordsReached = true;
					ReporterFacade.warning(String.format("No more records available, generated [%s] records.", i + 1));
					break;
				}
			}
			if (!maxRecordsReached) {
				ReporterFacade.info(String.format("Done. Generated [%s] records.", maxRecords));
			}
			recordWriter.close();
		} catch (Exception e) {
			ReporterFacade.error("Error occurred while generating records.", e);
		}
	}

}

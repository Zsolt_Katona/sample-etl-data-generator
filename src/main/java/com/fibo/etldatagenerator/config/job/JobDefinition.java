package com.fibo.etldatagenerator.config.job;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.fibo.etldatagenerator.reporter.ReporterFacade;

public class JobDefinition {
	
	private List<JobEntry<?>> entries;

	public JobDefinition() {
	}

	public JobDefinition(JobEntry<?>... entries) {
		this.entries = new ArrayList<>(entries.length);
		for (JobEntry<?> e : entries) {
			this.entries.add(e);
		}
	}
	
	public List<JobEntry<?>> getEntries() {
		return entries;
	}

	public void setEntries(List<JobEntry<?>> entries) {
		this.entries = entries;
	}
	
	public void execute() {
		Instant starts = Instant.now();
		for (JobEntry<?> entry : entries) {
			ReporterFacade.info(String.format("Starting job entry [%s].", entry.getDescription()));
			entry.execute();
		}
		Instant ends = Instant.now();
		ReporterFacade.info(String.format("The job took [%s] seconds.", Duration.between(starts, ends).getSeconds()));
	}

}

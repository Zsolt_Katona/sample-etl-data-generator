package com.fibo.etldatagenerator.config.format;

public class CSVFormat {
	
	public static final CSVFormat MS_EXCEL_COMPATIBLE = new CSVFormat(',', '"', '"', true, null);
	public static final CSVFormat OPENCSV_DEFAULT = new CSVFormat(',', '"', '\\', true, null);
	public static final CSVFormat SEPARATED_WITH_TABS_NO_QUOTING = new CSVFormat('\t', null, '\\', true, null);

	private Character separator;
	
	private Character quoteCharacter;
	
	private Character escapeCharacter;
	
	private boolean useHeader;
	
	private String nullRepresentation;
	
	public CSVFormat(Character separator, Character quoteCharacter, Character escapeCharacter, boolean useHeader,
			String nullRepresentation) {
		super();
		this.separator = separator;
		this.quoteCharacter = quoteCharacter;
		this.escapeCharacter = escapeCharacter;
		this.useHeader = useHeader;
		this.nullRepresentation = nullRepresentation;
	}

	public Character getSeparator() {
		return separator;
	}

	public Character getQuoteCharacter() {
		return quoteCharacter;
	}

	public Character getEscapeCharacter() {
		return escapeCharacter;
	}

	public boolean isUseHeader() {
		return useHeader;
	}

	public String getNullRepresentation() {
		return nullRepresentation;
	}
	
	
	
}

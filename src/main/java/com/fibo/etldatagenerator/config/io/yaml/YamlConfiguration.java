package com.fibo.etldatagenerator.config.io.yaml;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;
import com.fibo.etldatagenerator.config.job.JobDefinition;

public class YamlConfiguration {

	public static JobDefinition load(String pathToYamlFile) {
		try {
			YamlReader reader = new YamlReader(new FileReader(pathToYamlFile));
			JobDefinition ret = reader.read(JobDefinition.class);
			reader.close();
			return ret;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static void save(JobDefinition jobDefinition, String pathToYamlFile) {
		try {
			YamlWriter writer = new YamlWriter(new FileWriter(pathToYamlFile));
			writer.write(jobDefinition);
			writer.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
}

package com.fibo.etldatagenerator.generator.record;

import java.util.List;

public class Row {

	private String[] values;

	public Row(String[] values) {
		this.values = values;
	}
	
	public Row(List<Object> values) {
		super();
		this.values = values.stream().map(value -> value != null ? String.valueOf(value) : null).toArray(size -> new String[size]);
	}

	public String[] getValues() {
		return values;
	}
	
}

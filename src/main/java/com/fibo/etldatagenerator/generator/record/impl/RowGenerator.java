package com.fibo.etldatagenerator.generator.record.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.fibo.etldatagenerator.generator.record.RecordGenerator;
import com.fibo.etldatagenerator.generator.record.Row;
import com.fibo.etldatagenerator.generator.value.ValueGenerator;

@SuppressWarnings("rawtypes")
public class RowGenerator implements RecordGenerator<Row> {

	private String[] headerColumnNames;
	private ValueGenerator[] valueGenerators;
	
	public String[] getHeaderColumnNames() {
		return headerColumnNames;
	}

	public ValueGenerator[] getValueGenerators() {
		return valueGenerators;
	}

	public void setHeaderColumnNames(String[] headerColumnNames) {
		this.headerColumnNames = headerColumnNames;
	}

	public void setValueGenerators(ValueGenerator[] valueGenerators) {
		this.valueGenerators = valueGenerators;
	}

	@Override
	public void initialize() {
		for (ValueGenerator vg : valueGenerators) {
			vg.initialize();
		}
	}

	public Row headerRow() {
		return new Row(headerColumnNames);
	}
	
	public Row next() {
		List<Object> ret = new ArrayList<>(valueGenerators.length);
		for (ValueGenerator vg : valueGenerators) {
			ret.add(vg.nextValue());
		}
		return new Row(ret);
	}
	
	@Override
	public boolean hasNext() {
		for (ValueGenerator vg : valueGenerators) {
			if (!vg.hasNext()) return false;
		}
		return true;
	}

	public static class Builder {
		
		private List<String> headerColumnNames = new LinkedList<>();
		private List<ValueGenerator<?>> valueGenerators = new LinkedList<>();
		
		public static Builder create() {
			return new Builder();
		}
		
		public Builder add(String headerColumnName, ValueGenerator<?> valueGenerator) {
			headerColumnNames.add(headerColumnName);
			valueGenerators.add(valueGenerator);
			return this;
		}
		
		public RowGenerator build() {
			RowGenerator ret = new RowGenerator();
			ret.setHeaderColumnNames(headerColumnNames.toArray(new String[]{}));
			ret.setValueGenerators(valueGenerators.toArray(new ValueGenerator[]{}));
			ret.initialize();
			return ret;
		}
		
	}
	
}

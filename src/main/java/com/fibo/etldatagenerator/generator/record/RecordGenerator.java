package com.fibo.etldatagenerator.generator.record;

public interface RecordGenerator<T> {
	
	default void initialize() {
		// Nothing
	}
	
	T next();
	
	boolean hasNext();

}

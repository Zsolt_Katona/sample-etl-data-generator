package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class RandomEmailGenerator implements ValueGenerator<String> {

	private ValueGenerator<String> generator = new CompositeGenerator(
			new RandomGivenNameGenerator(),
			new ConstantGenerator<>("."),
			new RandomFamilyNameGenerator(),
			new RandomIntegerGenerator(1000, 9999),
			new ConstantGenerator<>("@"),
			new RandomLineFromClasspathResourceGenerator("data/free-email-providers.txt")
	);
	
	@Override
	public void initialize() {
		generator.initialize();
	}
	
	@Override
	public String nextValue() {
		return generator.nextValue();
	}

}

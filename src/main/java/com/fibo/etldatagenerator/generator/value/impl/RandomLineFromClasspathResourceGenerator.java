package com.fibo.etldatagenerator.generator.value.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import com.fibo.etldatagenerator.generator.RandomUtils;
import com.fibo.etldatagenerator.generator.value.ValueGenerator;

class RandomLineFromClasspathResourceGenerator implements ValueGenerator<String> {

	private String filePath;
	
	private String[] _values;
	
	public RandomLineFromClasspathResourceGenerator() {
	}

	public RandomLineFromClasspathResourceGenerator(String filePath) {
		this.filePath = filePath;
	}
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public void initialize() {
		try {
			_values = Files.lines(new File(ClassLoader.getSystemResource(filePath).getPath()).toPath()).toArray(size -> new String[size]);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String nextValue() {
		return _values[RandomUtils.random.nextInt(_values.length)];
	}

}

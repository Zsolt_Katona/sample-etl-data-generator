package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.RandomUtils;
import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class RandomStringGenerator implements ValueGenerator<String> {

	private RandomCharacterGenerator characterGenerator;
	private int minLength;
	private int maxLength;
	
	public RandomStringGenerator() {
	}

	public RandomStringGenerator(int length) {
		this.minLength = length;
		this.maxLength = length;
		this.characterGenerator = new RandomCharacterGenerator();
	}

	public RandomStringGenerator(int minLength, int maxLength) {
		this.minLength = minLength;
		this.maxLength = maxLength;
		this.characterGenerator = new RandomCharacterGenerator();
	}
	
	public RandomStringGenerator(int minLength, int maxLength, RandomCharacterGenerator characterGenerator) {
		this.minLength = minLength;
		this.maxLength = maxLength;
		this.characterGenerator = characterGenerator;
	}
	
	public RandomCharacterGenerator getCharacterGenerator() {
		return characterGenerator;
	}

	public void setCharacterGenerator(RandomCharacterGenerator characterGenerator) {
		this.characterGenerator = characterGenerator;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}
	
	@Override
	public void initialize() {
		characterGenerator.initialize();
	}

	@Override
	public String nextValue() {
		int length = RandomUtils.random.nextInt(maxLength - minLength + 1) + minLength;
		StringBuffer sb = new StringBuffer(length);
		for (int i = 0; i < length; ++i) sb.append(characterGenerator.nextValue());
		return sb.toString();
	}
	
}

package com.fibo.etldatagenerator.generator.value;

public interface ValueGenerator<T> {

	default void initialize() {
		// Nothing
	}
	
	T nextValue();
	
	default boolean hasNext() {
		// Default implementation for value generators providing infinite number of elements
		return true;
	}
	
}

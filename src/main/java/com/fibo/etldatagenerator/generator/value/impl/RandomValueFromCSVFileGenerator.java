package com.fibo.etldatagenerator.generator.value.impl;

import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import com.fibo.etldatagenerator.config.format.CSVFormat;
import com.fibo.etldatagenerator.generator.RandomUtils;
import com.fibo.etldatagenerator.generator.value.ValueGenerator;

import au.com.bytecode.opencsv.CSVReader;

public class RandomValueFromCSVFileGenerator implements ValueGenerator<String> {

	private String[] _values;
	
	private String inputFilePath;
	private int columnIndex;
	private boolean ignoreNullValues;
	private CSVFormat format;
	
	public RandomValueFromCSVFileGenerator() {
	}

	public RandomValueFromCSVFileGenerator(String inputFilePath, int columnIndex, boolean ignoreNullValues, CSVFormat format) {
		this.inputFilePath = inputFilePath;
		this.columnIndex = columnIndex;
		this.ignoreNullValues = ignoreNullValues;
		this.format = format;
	}
	
	public String getInputFilePath() {
		return inputFilePath;
	}

	public void setInputFilePath(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}

	public int getColumnIndex() {
		return columnIndex;
	}

	public void setColumnIndex(int columnIndex) {
		this.columnIndex = columnIndex;
	}

	public boolean isIgnoreNullValues() {
		return ignoreNullValues;
	}

	public void setIgnoreNullValues(boolean ignoreNullValues) {
		this.ignoreNullValues = ignoreNullValues;
	}

	public CSVFormat getFormat() {
		return format;
	}

	public void setFormat(CSVFormat format) {
		this.format = format;
	}

	@Override
	public void initialize() {
		try {
			CSVReader reader = new CSVReader(new FileReader(inputFilePath), toChar(format.getSeparator()),
					toChar(format.getQuoteCharacter()), toChar(format.getEscapeCharacter()));
			String[] line = null;
			boolean processingFirstRow = true;
			List<String> values = new LinkedList<>();
			while ((line = reader.readNext()) != null) {
				if (format.isUseHeader() && processingFirstRow) {
					// Do nothing
					processingFirstRow = false;
				} else {
					String value = line[columnIndex];
					if (value != null || !ignoreNullValues) {
						values.add(value);
					}
				}
			}
			this._values = values.toArray(new String[]{});
			reader.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private char toChar(Character value) {
		return value != null ? value.charValue() : '\0';
	}
	
	@Override
	public String nextValue() {
		return _values[RandomUtils.random.nextInt(_values.length)];
	}

}

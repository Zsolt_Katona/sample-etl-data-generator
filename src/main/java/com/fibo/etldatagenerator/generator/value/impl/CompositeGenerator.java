package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class CompositeGenerator implements ValueGenerator<String> {

	private ValueGenerator<?>[] generators;
	
	public CompositeGenerator() {
	}
	
	public CompositeGenerator(ValueGenerator<?>... generators) {
		this.generators = generators;
	}
	
	public ValueGenerator<?>[] getGenerators() {
		return generators;
	}

	public void setGenerators(ValueGenerator<?>[] generators) {
		this.generators = generators;
	}
	
	@Override
	public boolean hasNext() {
		for (ValueGenerator<?> vg : generators) {
			if (!vg.hasNext()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void initialize() {
		for (ValueGenerator<?> vg : generators) {
			vg.initialize();
		}
	}

	@Override
	public String nextValue() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < generators.length; ++i) {
			sb.append(generators[i].nextValue());
		}
		return sb.toString();
	}

}

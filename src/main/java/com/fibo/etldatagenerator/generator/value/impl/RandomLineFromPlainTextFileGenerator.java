package com.fibo.etldatagenerator.generator.value.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import com.fibo.etldatagenerator.generator.RandomUtils;
import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class RandomLineFromPlainTextFileGenerator implements ValueGenerator<String> {
	
	private String filePath;
	
	private String[] _values;
	
	public RandomLineFromPlainTextFileGenerator() {
	}

	public RandomLineFromPlainTextFileGenerator(String filePath) {
		try {
			_values = Files.lines(new File(filePath).toPath()).toArray(size -> new String[size]);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	@Override
	public void initialize() {
		try {
			_values = Files.lines(new File(filePath).toPath()).toArray(size -> new String[size]);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public String nextValue() {
		return _values[RandomUtils.random.nextInt(_values.length)];
	}
}

package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.RandomUtils;
import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class RandomIntegerGenerator implements ValueGenerator<Integer> {

	private int min = Integer.MIN_VALUE;
	private int max = Integer.MAX_VALUE;
	
	public RandomIntegerGenerator() {
	}

	public RandomIntegerGenerator(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	@Override
	public Integer nextValue() {
		return RandomUtils.random.nextInt(max - min + 1) + min;
	}

}

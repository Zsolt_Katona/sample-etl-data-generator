package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.RandomUtils;
import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class RandomCharacterGenerator implements ValueGenerator<Character> {

	public static final String LOWER_CASE_LETTERS = "abcdefghijklmnopqrstuvwxyz";
	public static final String UPPER_CASE_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String NUMBERS = "0123456789";
	public static final String SPECIAL_CHARACTERS = " =+-*/,.'\";:!?&<>#$%";
	public static final String BRACKETS = "()[]{}";

	public static final String ALPHANUMERICS = LOWER_CASE_LETTERS + UPPER_CASE_LETTERS + NUMBERS;
	
	private static final String DEFAULT = ALPHANUMERICS + SPECIAL_CHARACTERS + BRACKETS;
	
	private String characters = DEFAULT;
	
	private char[] _characters;
	
	public RandomCharacterGenerator() {
	}
	
	public RandomCharacterGenerator(String characters) {
		this.characters = characters;
	}
	
	@Override
	public void initialize() {
		this._characters = characters.toCharArray();
	}

	@Override
	public Character nextValue() {
		return _characters[RandomUtils.random.nextInt(_characters.length)];
	}

}

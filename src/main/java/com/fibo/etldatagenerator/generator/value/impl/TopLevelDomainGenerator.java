package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class TopLevelDomainGenerator implements ValueGenerator<String> {

	private ValueGenerator<String> generator = new RandomLineFromClasspathResourceGenerator("data/top-level-domains.txt");
	
	@Override
	public void initialize() {
		generator.initialize();
	}
	
	@Override
	public String nextValue() {
		return generator.nextValue();
	}

}

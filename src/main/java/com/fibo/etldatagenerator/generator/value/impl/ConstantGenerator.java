package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class ConstantGenerator<T> implements ValueGenerator<T> {

	private T constant;
	
	public ConstantGenerator() {
	}

	public ConstantGenerator(T constant) {
		this.constant = constant;
	}

	public T getConstant() {
		return constant;
	}

	public void setConstant(T constant) {
		this.constant = constant;
	}

	@Override
	public T nextValue() {
		return constant;
	}

}

package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.RandomUtils;
import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class Nuller<T> implements ValueGenerator<T> {

	private ValueGenerator<T> source;
	private double probability = 0;
	
	public Nuller(ValueGenerator<T> source, double probability) {
		this.source = source;
		this.probability = probability;
	}

	@Override
	public void initialize() {
		source.initialize();
	}

	@Override
	public boolean hasNext() {
		return source.hasNext();
	}

	@Override
	public T nextValue() {
		return RandomUtils.random.nextDouble() < probability ? null : source.nextValue();
	}

}

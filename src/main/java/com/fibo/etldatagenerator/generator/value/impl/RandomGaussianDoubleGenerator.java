package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.RandomUtils;
import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class RandomGaussianDoubleGenerator implements ValueGenerator<Double> {

	private double mean;
	private double deviation;
	
	public RandomGaussianDoubleGenerator() {
	}

	public RandomGaussianDoubleGenerator(double mean, double deviation) {
		this.mean = mean;
		this.deviation = deviation;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getDeviation() {
		return deviation;
	}

	public void setDeviation(double deviation) {
		this.deviation = deviation;
	}

	@Override
	public Double nextValue() {
		return RandomUtils.random.nextGaussian() * deviation + mean;
	}

}

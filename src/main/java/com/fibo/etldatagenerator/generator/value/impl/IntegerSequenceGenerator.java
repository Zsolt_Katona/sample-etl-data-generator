package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class IntegerSequenceGenerator implements ValueGenerator<Integer> {

	private int initialValue = 0;
	private int incrementBy = 1;
	private boolean allowOverflow = false;

	private int _value = 0;
	
	public IntegerSequenceGenerator() {
		this(0);
	}
	
	public IntegerSequenceGenerator(int initialValue) {
		this(initialValue, 1);
	}

	public IntegerSequenceGenerator(int initialValue, int incrementBy) {
		this(initialValue, incrementBy, false);
	}

	public IntegerSequenceGenerator(int initialValue, int incrementBy, boolean allowOverflow) {
		this.initialValue = initialValue;
		this.incrementBy = incrementBy;
		this.allowOverflow = allowOverflow;
	}
	
	public int getInitialValue() {
		return initialValue;
	}

	public void setInitialValue(int initialValue) {
		this.initialValue = initialValue;
	}

	public int getIncrementBy() {
		return incrementBy;
	}

	public void setIncrementBy(int incrementBy) {
		this.incrementBy = incrementBy;
	}

	public boolean isAllowOverflow() {
		return allowOverflow;
	}

	public void setAllowOverflow(boolean allowOverflow) {
		this.allowOverflow = allowOverflow;
	}

	@Override
	public void initialize() {
		if (incrementBy <= 0) {
			throw new IllegalArgumentException("The increment must be greater than zero.");
		}
		this._value = initialValue;
	}

	@Override
	public boolean hasNext() {
		return allowOverflow || _value + incrementBy > _value; // the latter condition is only true if there's no overflow
	}

	@Override
	public Integer nextValue() {
		int ret = _value;
		_value += incrementBy;
		return ret;
	}

}

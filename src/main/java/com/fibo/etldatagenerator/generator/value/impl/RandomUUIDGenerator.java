package com.fibo.etldatagenerator.generator.value.impl;

import java.util.UUID;

import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class RandomUUIDGenerator implements ValueGenerator<String> {

	@Override
	public String nextValue() {
		return UUID.randomUUID().toString();
	}

}

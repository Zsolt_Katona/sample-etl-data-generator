package com.fibo.etldatagenerator.generator.value.impl;

import com.fibo.etldatagenerator.generator.value.ValueGenerator;

public class RandomIPAddressGenerator implements ValueGenerator<String> {

	private ValueGenerator<String> generator = new CompositeGenerator(
			new RandomIntegerGenerator(0, 255),
			new ConstantGenerator<>("."),
			new RandomIntegerGenerator(0, 255),
			new ConstantGenerator<>("."),
			new RandomIntegerGenerator(0, 255),
			new ConstantGenerator<>("."),
			new RandomIntegerGenerator(0, 255)
	);
	
	@Override
	public void initialize() {
		generator.initialize();
	}
	
	@Override
	public String nextValue() {
		return generator.nextValue();
	}

}
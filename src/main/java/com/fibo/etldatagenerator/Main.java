package com.fibo.etldatagenerator;

import com.fibo.etldatagenerator.config.format.CSVFormat;
import com.fibo.etldatagenerator.config.io.yaml.YamlConfiguration;
import com.fibo.etldatagenerator.config.job.JobDefinition;
import com.fibo.etldatagenerator.config.job.JobEntry;
import com.fibo.etldatagenerator.generator.record.Row;
import com.fibo.etldatagenerator.generator.record.impl.RowGenerator;
import com.fibo.etldatagenerator.generator.value.impl.IntegerSequenceGenerator;
import com.fibo.etldatagenerator.generator.value.impl.RandomGivenNameGenerator;
import com.fibo.etldatagenerator.generator.value.impl.RandomIPAddressGenerator;
import com.fibo.etldatagenerator.generator.value.impl.RandomIntegerGenerator;
import com.fibo.etldatagenerator.generator.value.impl.RandomValueFromCSVFileGenerator;
import com.fibo.etldatagenerator.writer.RowWriter;
import com.fibo.etldatagenerator.writer.impl.CSVRowWriter;

public class Main {

	public static void main(String[] args) throws Exception {
		
		CSVFormat csvFormat = CSVFormat.SEPARATED_WITH_TABS_NO_QUOTING;
		
		RowGenerator rg = RowGenerator.Builder.create()
				.add("Id", new IntegerSequenceGenerator())
				.add("Name", new RandomGivenNameGenerator())
				.add("IP Address", new RandomIPAddressGenerator())
				.add("Rank", new IntegerSequenceGenerator(10, 5))
				.build();
		
		String filePath = System.getProperty("java.io.tmpdir") + "/sample-etl-data-generator-nodes.csv";
		RowWriter rw = new CSVRowWriter(filePath, csvFormat);
		
		JobEntry<Row> entry1 = new JobEntry<>("Nodes", rg, rw, 100000);
		
		RandomValueFromCSVFileGenerator randomValueFromCSVFileGenerator = 
				new RandomValueFromCSVFileGenerator(filePath, 0, true, csvFormat);
		RowGenerator rg2 = RowGenerator.Builder.create()
				.add("Source Id", randomValueFromCSVFileGenerator)
				.add("Target Id", randomValueFromCSVFileGenerator)
				.add("Measure", new RandomIntegerGenerator(1, 5))
				.build();
		
		String filePath2 = System.getProperty("java.io.tmpdir") + "/sample-etl-data-generator-edges.csv";
		RowWriter rw2 = new CSVRowWriter(filePath2, csvFormat);
		
		JobEntry<Row> entry2 = new JobEntry<>("Edges", rg2, rw2, 300000);

		JobDefinition jobDefinition = new JobDefinition(entry1, entry2);

		// YamlConfiguration.save(jobDefinition, "jobdef.yml"); FIXME it's not working yet
		
		jobDefinition.execute();
		
	}
	
}
